package com.jpmc.nycschool.domain.usecase

import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.domain.repository.ISchoolRepository
import javax.inject.Inject

interface SchoolApplicationUseCase<Request, Response> {
    suspend fun invoke(request: Request): Response
}
/*
interface SchoolApplicationUseCase1<Response> {
    suspend fun invoke(): Response
}
interface SchoolApplicationUseCase2<Request> {
    suspend fun invoke(request: Request)
}

* we can define structure of Usecase using above interface to follow for future development of usecases.
* */

class GetNycSchoolUseCase @Inject constructor(
    private val schoolRepository: ISchoolRepository
) : SchoolApplicationUseCase<Unit?, ArrayList<SchoolDataItem>> {

    override suspend fun invoke(request: Unit?): ArrayList<SchoolDataItem> {
        return schoolRepository.getSchools()
    }
}