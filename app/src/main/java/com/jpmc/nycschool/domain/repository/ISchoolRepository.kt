package com.jpmc.nycschool.domain.repository

import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.data.model.SchoolSatScoreResponse

interface ISchoolRepository {

    suspend fun getSchools(): ArrayList<SchoolDataItem>

    suspend fun getSATInfoFromSchool(dbn: String): ArrayList<SchoolSatScoreResponse>
}