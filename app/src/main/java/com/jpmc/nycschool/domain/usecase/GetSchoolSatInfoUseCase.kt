package com.jpmc.nycschool.domain.usecase

import com.jpmc.nycschool.data.model.SchoolSatScoreResponse
import com.jpmc.nycschool.domain.repository.ISchoolRepository
import javax.inject.Inject

class GetSchoolSatInfoUseCase @Inject constructor(
    private val schoolRepository: ISchoolRepository,
) {
    suspend operator fun invoke(dbn: String): ArrayList<SchoolSatScoreResponse> {
        return schoolRepository.getSATInfoFromSchool(dbn)
    }
}