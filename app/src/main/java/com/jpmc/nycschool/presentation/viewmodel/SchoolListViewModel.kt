package com.jpmc.nycschool.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.nycschool.data.model.SchoolDataItem
import com.jpmc.nycschool.domain.usecase.GetNycSchoolUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/*class MyViewModelScope : CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext = Dispatchers.Main + job
    fun onDestroy() {
        job.cancel()
    } // Cancel the job when the ViewModelStoreOwner is destroyed    } }
}*/

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val schoolUseCase: GetNycSchoolUseCase

    ) :
    ViewModel() {
//    var myViewModelScope: CoroutineScope = MyViewModelScope()
    val schoolInfoLiveData = MutableLiveData<ArrayList<SchoolDataItem>>()
    val errorLiveData = MutableLiveData<String>("")

    fun getSchools() {
        viewModelScope.launch {
            try {
                //here i would prefer to use sealed class for different response like Success, Progress, Error etc to manage all the scenario
                schoolInfoLiveData.postValue(schoolUseCase.invoke(null))
            } catch (e: Exception) {
                println(e.localizedMessage)
                errorLiveData.postValue("Something went wrong")
            }
        }

    }

    /*override fun onCleared() {
        super.onCleared()
        (myViewModelScope as? MyViewModelScope)?.onDestroy()
    }*/ // Cancel the CoroutineScope when the ViewModel is cleared    }
}